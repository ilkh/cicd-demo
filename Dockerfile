FROM docker.io/python:3.8-slim
COPY app/requirements.txt /opt/app/
WORKDIR /opt/app/
RUN pip install -r requirements.txt
COPY app/ /opt/app/
ENTRYPOINT ["gunicorn"]
CMD ["-c", "/opt/app/gunicorn.conf.py", "app:app"]

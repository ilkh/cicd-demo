from setuptools import setup

setup(
    name='cicd-demo',
    packages=['cicd-demo'],
    include_package_data=True,
    install_requires=[
        'flask',
        'gunicorn',
    ],
)
